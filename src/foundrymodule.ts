class PdfImporter {
    constructor() {
        Hooks.once('init', () => {
            this.hookImportButton();
        });
    }

    hookImportButton() {
        Hooks.on('renderSettings', (app, html, data) => {
            console.log('adding button');
            const importButton = $(`<button class="bestiary-browser-btn"><i class="fas fa-fire"></i> PDF Importer</button>`);

            if (game.user.isGM) {
                html.find('#pf2e-player-config').after(importButton);
            }

            // Handle button clicks
            importButton.click((ev) => {
                ev.preventDefault();
                this.importFromPDFDialog();
            });
        });
    }

    async importFromPDFDialog() {
        new Dialog({
            title: `Import PDF`,
            content: await renderTemplate("modules/pdftofoundry/templates/import-window.html", {}),
            buttons: {
                import: {
                    icon: '<i class="fas fa-file-import"></i>',
                    label: "Import PDF",
                    callback: async (html) => {
                        if (html instanceof HTMLElement) return;
                        const form = html.find("form")[0];
                        if (!form.data.files.length) return ui.notification.error("You did not upload a data file!");
                        //readTextFromFile(form.data.files[0]).then(json => this.importFromJSON(json));

                        const importer = await import(/* webpackChunkName: "importer-chunk" */ './importer');
                        importer.generateData(form.data.files[0]);
                    }
                },
                no: {
                    icon: '<i class="fas fa-times"></i>',
                    label: "Cancel"
                }
            },
            default: "import"
        }, {
            width: 400
        }).render(true);
    }
}

const pdfImporter = new PdfImporter();