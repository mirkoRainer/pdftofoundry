import * as PDFJS from "pdfjs-dist";

import FileSaver = require("file-saver");
import JSZip = require("jszip");
import rangeParser from 'parse-numeric-range';

require('./style.scss');

import * as Foundry from '../foundry'
import { parsePdf, TextDetail } from '../parse'
import adventures from '../modules'
import { ModuleDescription, ModuleData } from "../modules/IAdventure";
import { ModuleImporter } from "../modules/ModuleImporter";
import { IModuleOutput } from "../IModuleOutput";

const urlParams = new URLSearchParams(window.location.search);

const parseDebug = urlParams.get('parseDebug') == 'true';
const imageDebug = urlParams.get('imageDebug') == 'true';
const debug = urlParams.get('debug') ?? true;
const pages = urlParams.get('pages') !== null ? rangeParser(urlParams.get('pages'), 10) : undefined;

var pdfPath : string = urlParams.get('pdfPath') !== null ? urlParams.get('pdfPath') : undefined;
var forceAdventure : number = urlParams.get('forceAdventure') !== null ? parseInt(urlParams.get('forceAdventure'), 10) : undefined;

console.clear();


PDFJS.GlobalWorkerOptions.workerSrc = "./pdf.worker.bundle.js";

function progressFn(status: string, progress: number) {
    const dropAreaText = document.getElementById('dropLocationText');
    dropAreaText.innerText = status;

    const dropAreaProgress = document.getElementById('progressBar') as HTMLProgressElement;
    dropAreaProgress.value = progress;

    //console.log(status);
}

function makeid() {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < 16; i++) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

class WebModuleOutput implements IModuleOutput {
    prefix: string;
    actors: Foundry.HazardActor[] = [];
    journals: Foundry.Journal[] = [];
    scenes: Foundry.Journal[] = [];

    constructor(prefix: string) {
        this.prefix = prefix;
    }

    progressFn(status: string, progress: number) {
        progressFn(status, progress);
    }

    async emitFile(path: string, name: string, data: string | Blob) {
        zip.file(name, data);
        return `${path}/${name}`;
    }

    async createScene(scene: any) {
        scene._id = makeid();
        this.scenes.push(scene);        
    }
    
    async createJournal(journal: Foundry.Journal) {
        journal._id = makeid();
        this.journals.push(journal);
    }

    async createActor(actor: Foundry.HazardActor) {
        actor._id = makeid();
        this.actors.push(actor);
    }

    finalize() {
        zip.file(this.prefix + '/packs/journals.db', this.journals.map(e => JSON.stringify(e) + '\n').reduce((c, x) => c + x, ''));
        zip.file(this.prefix + '/packs/actors.db', this.actors.map(e => JSON.stringify(e) + '\n').reduce((c, x) => c + x, ''));
        zip.file(this.prefix + '/packs/scenes.db', this.scenes.map(e => JSON.stringify(e) + '\n').reduce((c, x) => c + x, ''));
        //createModuleJson();
    }
}

var zip = new JSZip();

function createModuleJson(adventureModule: ModuleDescription) {
    let moduleJson : Foundry.ModuleInfo = {
        "name": adventureModule.prefix,
        "title": adventureModule.fullName,
        "description": "",
        "version": "1.0.0",
        "author": "fryguy",
        "minimumCoreVersion": "0.4.7",
        "compatibleCoreVersion": "0.5.5",
        "packs": [
            {
                "name": "journals",
                "label": `${adventureModule.fullName} Journals`,
                "path": "packs/journals.db",
                "entity": "JournalEntry",
                "module": adventureModule.prefix
            },
            {
                "name": 'scenes',
                "label": `${adventureModule.fullName} Scenes`,
                "path": 'packs/scenes.db',
                "entity": "Scene",
                "module": adventureModule.prefix
            },
            {
                "name": 'actors',
                "label": `${adventureModule.fullName} Scenes`,
                "path": 'packs/actors.db',
                "entity": "Actor",
                "module": adventureModule.prefix
            }
        ]
    };

    zip.file(adventureModule.prefix + '/module.json', JSON.stringify(moduleJson));
}

async function generateModule(pdfPath: string) {
    let parsedPdf = await parsePdf(pdfPath, progressFn, pages);

    let adventureModule : ModuleDescription;
    if (forceAdventure !== undefined) {
        adventureModule = adventures[forceAdventure];
    } else {
        adventureModule = adventures.find(a => a.detect(parsedPdf));
    }

    if (adventureModule === undefined) {
        progressFn('failed to detect pdf. cannot proceed :(', 0);
        return;
    }

    let output = new WebModuleOutput(adventureModule.fullName);

    let importer = new ModuleImporter();
    const success = await importer.import(adventureModule, parsedPdf, output);
    if (!success) return;

    if (debug) {
        let pageContainer = document.getElementById('pageContainer');
        for (let entry of output.journals) {
            pageContainer.innerHTML += `<h1>${entry.name}</h1> <p>${entry._id}</p> ${entry.content} <hr />`;
        }

        for (let scene of output.scenes) {
            pageContainer.innerHTML += `<h1>${scene.name}</h1> <pre>${JSON.stringify(scene, null, 4)}</pre> <hr />`;
        }

        for (let actor of output.actors) {
            pageContainer.innerHTML += `<h1>${actor.name}</h1> <p>${actor._id}</p> <pre>${JSON.stringify(actor, null, 4)}</pre> <hr />`;
        }
    }

    if (parseDebug) {
        for (const item of parsedPdf.textDetails) {
            console.log(`${item.page} - (${item.x}, ${item.y}) - ${item.fontId} - ${item.text}`);
        }
    }

    if (imageDebug) {
        let imageNum = 0;
        for (let image of parsedPdf.imageDetails) {
            if (image.width == 2550 && image.height == 1669) { continue; }
            if (image.width == 1261 && image.height == 551) { continue; }
            if (image.width == 1261 && image.height == 418) { continue; }
            if (image.width == 1258 && image.height == 563) { continue; }
            if (image.width == 218 && image.height == 122) { continue; }
            if (image.width == 540 && image.height == 183) { continue; }
            if (image.width == 780 && image.height == 436) { continue; }
            if (image.width == 1299 && image.height == 155) { continue; }
            if (image.width == 1299 && image.height == 174) { continue; }
            if (image.width == 2555 && image.height == 1674) { continue; }
            if (image.width == 461 && image.height == 555) { continue; }
            if (image.width < 150 || image.height < 150) { continue; }

            let imageDebug = document.createElement("div");

            let desc = document.createElement("input")
            desc.style.display = 'block';
            desc.type = 'text';
            desc.onclick = (ev: MouseEvent) => { desc.focus(); desc.select(); };
            desc.value = `page: ${image.page}, width: ${image.width}, height: ${image.height}`;
            imageDebug.appendChild(desc);

            let canvas = await image.getImageCanvas();
            canvas.style.cssText = '';
            imageDebug.appendChild(canvas);

            document.getElementById('pageContainer').appendChild(imageDebug);
        }
    }

    output.progressFn('success. download now!', 1);

    const downloadButton = document.getElementById('downloadButton') as HTMLButtonElement;
    downloadButton.disabled = false;
    downloadButton.onclick = () => {
        zip.generateAsync({type:'blob'})
        .then(blob => FileSaver.saveAs(blob, `${adventureModule.prefix}.zip`));
    };
}


if (pdfPath !== undefined) {
    generateModule(pdfPath);
}

window.onload = (_:any) => {
    const dropArea = document.getElementById('dropLocation');

    ['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
        dropArea.addEventListener(eventName, (e: any) => {
            e.preventDefault()
            e.stopPropagation()
        }, false);
    });

    ['dragenter', 'dragover'].forEach(eventName => {
        dropArea.addEventListener(eventName, e => dropArea.classList.add('has-background-success'), false);
    });

    ['dragleave', 'drop'].forEach(eventName => {
        dropArea.addEventListener(eventName, e => dropArea.classList.remove('has-background-success'), false);
    });


    dropArea.addEventListener('drop', handleDrop, false);
    function handleDrop(e: any) {
        e.preventDefault();
        const file = e.dataTransfer.files[0];
        const url = URL.createObjectURL(file);
        generateModule(url);
    }
};
