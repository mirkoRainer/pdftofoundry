import { ModuleDescription, ModuleData, AdventureImage, CaptionImage } from "./IAdventure";
import { ModuleScene, ModuleSceneTiled } from "./ModuleScene";
import { PdfData } from "../parse";
import * as PFS from "./pfs";

const adventureModule : ModuleDescription = {
    prefix: "pfs-1-06",
    fullName: "PFS #1-06 - Lost on the Spirit Road",
    detect: (pdf: PdfData) => {
        return pdf.textDetails.some(t => t.text == 'PATHFINDER SOCIETY SCENARIO #1–06' && t.fontId == 'GoodOT-CondBold:10' && t.page == 1);
    },
    extractDetails: PFS.extractDetails,
    pages: [
        new PFS.TwoColumnWithRightCutout(3, 550),
        new PFS.TwoColumn(4),
        new PFS.TwoColumn(5),
        new PFS.TwoColumn(6),
        // 7 is map
        new PFS.TwoColumn(8),
        new PFS.TwoColumn(9),
        // 10 is map
        new PFS.TwoColumn(11),
        new PFS.TwoColumn(12),
        new PFS.TwoColumn(13),
        // 14 is map
        new PFS.TwoColumn(15),

        // appendix
        new PFS.TwoColumn(16, 'appendix1'),
        new PFS.TwoColumn(17, 'appendix1'),
        
        new PFS.TwoColumnWithRightCutout(18, 550, 'appendix2'),
        new PFS.TwoColumn(19, 'appendix2'),
        new PFS.TwoColumnWithRightCutout(20, 550, 'appendix2'),

        new PFS.TwoColumn(21, 'appendix3'),
        new PFS.TwoColumn(22, 'appendix3'),
        new PFS.TwoColumn(23, 'appendix3'),

        new PFS.TwoColumn(24, 'appendix4'),
        new PFS.TwoColumn(25, 'appendix4'),
        new PFS.TwoColumn(26, 'appendix4'),

        new PFS.TwoColumn(27, 'appendix5'),
        new PFS.TwoColumn(28, 'appendix5'),
        new PFS.TwoColumn(29, 'appendix5'),

        new PFS.TwoColumn(30, 'appendix6'),
        new PFS.TwoColumn(31, 'appendix6'),
        new PFS.TwoColumn(32, 'appendix6'),

        new PFS.TwoColumn(33, 'appendix7'),
        new PFS.TwoColumn(34, 'appendix7'),
        new PFS.TwoColumn(35, 'appendix7'),

        new PFS.TwoColumn(36, 'appendix8'),
        new PFS.TwoColumn(37, 'appendix8'),
        new PFS.TwoColumn(38, 'appendix8'),

        new PFS.TwoColumn(39, 'appendix9'),
        new PFS.TwoColumn(40, 'appendix9'),
        new PFS.TwoColumn(41, 'appendix9'),
    ],

    scenes: [
        new ModuleScene({
            name: 'map1',
            page: 7, width: 1044, height: 1295,
            imageLeft: 21, imageTop: 21, imageRight: 1023, imageBottom: 1274,
            imageRows: 30, imageCols: 24
        }),
        new ModuleSceneTiled({
            name: 'map2',
            page: 10, width: 265, height: 265,
            imageLeft: 8, imageTop: 8, imageRight: 256, imageBottom: 256,
            imageRows: 6, imageCols: 6,
            numTileCols: 4, numTileRows: 5,
            tileRotations: [
                [3, 3, 3, 2],
                [0, 3, 0, 1],
                [1, 1, 2, 3],
                [1, 3, 0, 0],
                [2, 1, 3, 1]
            ]
        }),
        new ModuleSceneTiled({
            name: 'map3',
            page: 14, width: 265, height: 265,
            imageLeft: 8, imageTop: 8, imageRight: 256, imageBottom: 256,
            imageRows: 6, imageCols: 6,
            numTileCols: 4, numTileRows: 5,
            tileRotations: [
                [1, 0, 0, 1],
                [0, 0, 2, 0],
                [3, 1, 1, 0],
                [0, 0, 2, 0],
                [2, 0, 3, 2]
            ]       
        }),
    ],
    journals: [
        new AdventureImage({ name: 'journal1', page: 1, width: 1261, height: 1253 }),
        new AdventureImage({ name: 'journal2', page: 2, width: 820, height: 281 }),
        new AdventureImage({ name: 'journal3', page: 3, width: 409, height: 278 }),
        new CaptionImage({ name: 'npc1', page: 4, width: 527, height: 527 }),
        new CaptionImage({ name: 'npc2', page: 15, width: 592, height: 850 }),
        new CaptionImage({ name: 'npc3', page: 42, width: 1206, height: 1193 }),
        new CaptionImage({ name: 'npc4', page: 43, width: 1096, height: 1636 }),
        new CaptionImage({ name: 'npc5', page: 44, width: 948, height: 1149 }),
        new CaptionImage({ name: 'npc6', page: 45, width: 1239, height: 749 }),
        new CaptionImage({ name: 'npc7', page: 46, width: 1022, height: 1133 }),
    ],
    finalize: (moduleData: ModuleData) => {
    }
};

export default adventureModule;