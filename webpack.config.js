var webpack = require("webpack"); // eslint-disable-line no-unused-vars
var path = require("path");
var fs = require('fs-extra');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const WriteFilePlugin = require('write-file-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const ProgressBarPlugin = require('progress-bar-webpack-plugin');

function getFoundryConfig() {
    const configPath = path.resolve(process.cwd(), 'foundryconfig.json');
    let config;

    if (fs.existsSync(configPath)) {
        config = fs.readJSONSync(configPath);
        return config;
    }
}

const foundryConfigJson = getFoundryConfig();
const foundryDist = foundryConfigJson !== undefined ? path.join(foundryConfigJson.dataPath, 'Data', 'modules', foundryConfigJson.systemName) : path.join(__dirname, 'dist_foundry');

let config = {
    context: __dirname,
    entry: {
        main: "./src/web/app.ts",
        "pdf.worker": "pdfjs-dist/build/pdf.worker.entry",
    },
    mode: "none",
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader'
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true,
                        }
                    }
                ]
            }
        ],
    },
    plugins: [
        new CopyWebpackPlugin([
            { from: 'static_web' }
        ], {
            writeToDisk: true
        }),
        new WriteFilePlugin(),
        new MiniCssExtractPlugin({
            filename: 'css/style.css'
        }),
        new ProgressBarPlugin()
    ],
    resolve: {
        extensions: ['.tsx', '.ts', '.js']
    },
    output: {
        path: path.join(__dirname, 'dist'),
        filename: "[name].bundle.js",
    },
};

let foundryConfig = {
    context: __dirname,
    entry: {
        main: "./src/foundrymodule.ts",
        "pdf.worker": "pdfjs-dist/build/pdf.worker.entry",
    },
    mode: "none",
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader'
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true,
                        }
                    }
                ]
            }
        ],
    },
    plugins: [
        new CopyWebpackPlugin([
            { from: 'static_module' }
        ], {
            writeToDisk: true
        }),
        new WriteFilePlugin(),
        new MiniCssExtractPlugin({
            filename: 'css/style.css'
        }),
        new ProgressBarPlugin()
    ],
    resolve: {
        extensions: ['.tsx', '.ts', '.js']
    },
    output: {
        path: foundryDist,
        filename: "[name].bundle.js",
    },
};


module.exports = (env, argv) => {
    const performance = {
        hints: false,
        maxEntrypointSize: 1000000,
        maxAssetSize: 1000000
    };
    if (argv.mode === 'production') {
        config.performance = performance;
        foundryConfig.performance = performance;
    } else {
        config.devtool = 'inline-source-map';
        config.devServer = {
            contentBase: path.join(__dirname, 'dist'),
            compress: true,
            //host: '0.0.0.0',
            port: 8080,
            disableHostCheck: true
        };
        foundryConfig.devtool = 'inline-source-map';
        foundryConfig.watch = true;
    }

    if (env !== undefined && env.web) {
        return config;
    }

    return foundryConfig;
};